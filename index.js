//Setup the dependencies
const express = require("express");
const mongoose =  require("mongoose");

// This will allow us to use
const taskRoute = require("./routes/taskRoute")

//Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Database Connection
mongoose.connect("mongodb+srv://Admin:admin123@course-booking.cevr7.mongodb.net/B157_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Add the task route
// This will allow all the task routes create
app.use("/tasks", taskRoute);
// http:localhost: 4000/tasks/

app.listen(port, () => console.log(`Now listening to port ${port}`));
